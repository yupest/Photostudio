<!DOCTYPE html>
<html>
<head>
  <title>Фотостудия</title>  
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/style.css" rel="stylesheet">
  <script>
    function Select(Element)
    {
      if (Element.classList) {
        Element.classList.toggle("check");
      }
      return false;
    }
  </script>
    <?php
    include_once "../app/Database.php";
      include_once "../app/model/base_photostudio.php";
      include_once "../app/model/order.php";
    ?>
</head>
<body>
  <div class="container" style="background: #fff;">
    <p style="text-align: right;"><a href="/photostudio/index.php">Выход <span class="glyphicon glyphicon-log-out"></span></a></p>
    <?php 
      $order = new Order();
      if (empty($_GET['certificate']) && $_GET['photographer']=='on')
      {
        $order->choose_certificates();
      }
      else
      {
        if (empty($_GET['hall']))
        {
          $order->choose_halls();
        }
        else
        {
          if (empty($_GET['date']) && empty($_POST['date']))
          {
            $order->choose_date();
          }
          else
          {
            if (empty($_GET['duration']))
            {
              $order->select_time();
            }
            else
            {
              if (empty($_GET['action']) || $_GET['action'] =='validate')
              {
                $order->personal_data();
              }
              else if ($_GET['action'] =='insert')
              {
                $order->add_order();
                if ($_GET['photographer']=='on')
                {
                  $order->add_value('ordering_the_certificate');
                  $order->add_photographer();
                }
                $order->add_value('duration_of_photo_shoot');
                echo '<h2>Спасибо за вашу оставленную заявку! Мы с вами свяжемся :)</h2>';
              }
            }
          }
        }
      }
    ?>
  </div>
</body>