<?php
class Database {

    protected static $_pdo;

    public static function get_pdo()
    {
      	
        if (empty(static::$_pdo))
        {
            $config =
              [
                'host'   => 'localhost',
                'dbname' => 'photostudio',
                'user'   => 'root',
                'passw'  => '123',
            ];

            static::$_pdo = new PDO('mysql:host=' . $config['host'] .';dbname=' . $config['dbname'],
                $config['user'], 
                $config['passw']);
        }
				static::$_pdo->exec('SET NAMES "utf8";');
        return static::$_pdo;
    }

}