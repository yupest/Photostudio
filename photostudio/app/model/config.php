﻿<?php 
function get_attributes($table)
{
  switch($table)
  {    
    case 'photographer':
      $properties=
        [
      		'id',
        	'name',
        	'surname',
        	'price',
      	];
    	break;
    case 'duration_of_photo_shoot':
      $properties=
        [
          'id',
          'id_order',
          'duration',
        ];
      break;
    case 'shedule_photographer':
      $properties=
        [
      		'id',
        	'id_ordering_the_certificate',
        	'id_photographer',
        ];
    	break;
    case 'rent_of_equipment':
      $properties=
        [
      		'id',
        	'id_equipment',
        	'id_order',
      	];
    	break;
    case 'certificate':
      $properties=
        [
      		'id',
        	'name',
        	'price',
      	];
    	break;
    case 'ordering_the_certificate':
      $properties=
        [
      		'id',
        	'id_certificate',
        	'id_order',
      	];
    	break;
    case 'order':
      $properties=
        [
      		'id',
        	'id_client',
        	'id_hall',
        	'date',
        	'time',
        	'id_event',
        	'created_at',
      	];
    	break;
    case 'client':
      $properties=
        [
      		'id',
        	'name',
        	'surname',
        	'passport',
        	'phone',
      	];
    	break;
    default :
    	$properties=
        [
      		'id',
        	'name'
      	];
      break;

  }    	
  return $properties;
}