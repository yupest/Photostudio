﻿<?php
function get_ru($word)
{
  switch($word)
  {    
    case 'client':
      return "Заказчики";
    case 'equipment':
      return "Оборудование";
    case 'event':
      return "Мероприятия";
    case 'hall':
      return "Залы";
    case 'photographer':
      return "Фотографы";  
    case 'rent_of_equipment':
      return "Аренда оборудования";
    case 'order':
      return "Заказы";
    case 'certificate':
      return "Сертификаты";
    case 'shedule_photographer':
      return "График работы фотографов";
    case 'ordering_the_certificate':
      return "Заказанные сертификаты"; 
    case 'duration_of_photo_shoot':
      return "Длительность фотосессии";   
    
  }
}
  function main_table($str)
  {
    $name = new Photostudio($str);    
    if (!empty($_GET['action']))
    {
      switch ($_GET['action'])
      {
        case 'add':
          $name->set_url("/photostudio/admin/index.php?".$str."&action=validate&type=create");
          include 'forms/form.php';
          break;
        case 'delete':
          $name->delete_at();
          break;
        case 'reestablish':
          $name->reestablish();
          break;
        case 'edit':
          $name->set_url("/photostudio/admin/index.php?".$str."&action=validate&type=edit&id=".$_GET['id']);
          include 'forms/form.php';
          break;
        case 'insert':
        case 'update':
          echo "Успешно!";
          break;
        case 'sort':
          $name->order_by();
          break;
        case 'validate':
          $name->validate($_GET['type']);
          include 'forms/form.php';
          break;
      }
      if ($_GET['action']!='add' && $_GET['action']!='edit' && $_GET['action']!='update' && $_GET['action']!='insert' && $_GET['action']!='validate')
      {
        echo '<div class="row">';
        $name->make_search();
        echo '</div>';
        $name->make_table();
      }
    }
    else
    {
      echo '<div class="row">';
      $name->make_search();
      echo '</div>';
      $name->make_table();
    }
  }