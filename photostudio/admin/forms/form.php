﻿<!DOCTYPE html>
<html>
<head>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
  <div class="container">
    <h3>
      Введите данные:
    </h3>
    <form class="form-horizontal" method ="post" action="<?= $name->url?>">
      <?php
      if (isset($_GET['type']))
      {
        $method=$_GET['type'];
      }
      else
      {
        $method=$_GET['action'];
      }
        $name->make_form($method);
      ?>
    </form>
  </div>
</body>
</html>